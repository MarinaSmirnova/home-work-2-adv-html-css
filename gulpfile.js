const gulp = require('gulp');
const htmlmin = require('gulp-htmlmin');
const concat = require('gulp-concat');
const cleanCSS = require('gulp-clean-css');
const imagemin = require('gulp-imagemin');
const autoprefixer = require('gulp-autoprefixer');
const gulpJsMinify = require('gulp-js-minify');
const sass = require('gulp-sass')(require('sass'));
const clean = require('gulp-clean');
const browserSync = require('browser-sync').create();

const cleanDist = () => {
    return gulp.src('./dist', {read: false})
    .pipe(clean())
}

const dev = () => {
browserSync.init({
    server: {
        baseDir: "./dist"
    }
});

gulp.watch('./src/**/*', gulp.series(cleanDist, gulp.parallel(html, js, scss, img, fonts), (next) => {
    browserSync.reload()
    next();
}))
}

const html = () => {
        return gulp.src("./src/**/*.html")
        .pipe(htmlmin({ collapseWhitespace: true }))
        .pipe(gulp.dest("./dist"));
    };

const js = () => {
        return gulp.src("./src/**/*.js")
        .pipe(concat("scripts.min.js"))
        .pipe(gulpJsMinify())
        .pipe(gulp.dest("./dist/js"));
    };

const scss = () => {
       return gulp.src('./src/scss/main.scss')
      .pipe(sass.sync().on('error', sass.logError))
      .pipe(autoprefixer())
      .pipe(concat('styles.min.css'))
      .pipe(cleanCSS({compatibility: 'ie8'}))
      .pipe(gulp.dest('./dist/css'))
};

const img = () => {
    return gulp.src('./src/**/*.+(png|jpg|svg|jpeg|webp)')
    .pipe(imagemin())
    .pipe(gulp.dest('./dist'))
};

const fonts= () => {
    return gulp.src('./src/**/*.+(eot|woff|woff2|ttf|otf)')
    .pipe(gulp.dest('./dist'))
};

gulp.task('html', html)
gulp.task('js', js)
gulp.task('scss', scss)
gulp.task('img', img)
gulp.task('fonts', fonts)
gulp.task('clean', cleanDist)

gulp.task('build', gulp.series(cleanDist, gulp.parallel(html, js, scss, img, fonts)))
gulp.task('dev', dev)
