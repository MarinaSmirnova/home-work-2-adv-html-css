const headerBtn = document.getElementById("header_btn");
const headerList = document.getElementById("header_list");

headerBtn.addEventListener("click", () => {
    headerList.classList.toggle("header-menu__list--on");
    headerBtn.classList.toggle("header-menu__burger-btn--on");
  });

